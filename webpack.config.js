let path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');


const htmlPlugin = new HtmlWebPackPlugin({
	template: './index.html',
	filename: './index.html'
});

module.exports = {
	mode: 'development',
	entry: './src/index.tsx',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js',
        publicPath: ""
    },
	resolve: {
		extensions: ['.webpack.js', '.ts', '.tsx', '.js', '.json', '.css']
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/, loader: 'ts-loader'
			},

			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components|public)/,
				loader: 'babel-loader'
			},
			]
	},
    devServer: {
        historyApiFallback: true,
    },

	plugins: [htmlPlugin]
};
