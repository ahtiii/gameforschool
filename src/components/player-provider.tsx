import {Component, ReactNode} from "react";
import * as React from "react";
import {observer, Provider} from "mobx-react";
import MainRouter from "./main-router";
import PlayerStore from "../stores/player-store";
import {css} from "emotion";

const className = css`
	text-align: center;
	position: absolute;
	display: flex;
	justify-content: center;
	align-items: center;
	height: 100%;
	width: 100%;
	top:0;
	left:0;
	`;
@observer
export default class PlayerProvider extends Component {
	public player1: PlayerStore = new PlayerStore(1);
	public player2: PlayerStore = new PlayerStore(2);

	public render(): ReactNode {

		return (
				<Provider player1={this.player1} player2={this.player2}>
					<div className={className}>
						<MainRouter/>
					</div>
				</Provider>
		)
	}
}
