import {Component, ReactNode} from "react";
import {observable} from "mobx";
import Overlay from "./forms/overlay";
import * as React from "react";
import OverlayStore from "../stores/overlay-store";
import {observer} from "mobx-react";

export interface OverlayRendererProps {
	store: OverlayStore
}

@observer
export default class OverlayRenderer extends Component<OverlayRendererProps> {

	@observable.ref
	private renderOverlays = () => {
		return
	};

	public render(): ReactNode {
		return (
			this.props.store.allOverlays.map((overlay) => {
				return <Overlay key={overlay.id} id={overlay.id} {...overlay} />
			})
		)
	}
}
