var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { css, cx } from 'emotion';
import * as _ from 'lodash';
import MatchStick from './match-stick';
import * as React from 'react';
import { Component } from 'react';
import { inject, observer } from 'mobx-react';
import facepaint from 'facepaint';
let Rows = class Rows extends Component {
    constructor() {
        super(...arguments);
        this.createMatchstickPyramid = () => {
            const { gameStore } = this.props;
            if (gameStore) {
                return gameStore.rowsWithCount.map((row, rowkey) => {
                    return (React.createElement("div", { className: cx(rowCss), key: rowkey }, _.range(row.count).map((stick, key) => {
                        return (React.createElement(MatchStick, { key: key, onClick: () => {
                                if (!gameStore.notClickable) {
                                    gameStore.updateRow(rowkey);
                                    this.createMatchstickPyramid();
                                }
                            } }));
                    })));
                });
            }
            return null;
        };
    }
    render() {
        return (React.createElement("div", { className: rowContainer }, this.createMatchstickPyramid()));
    }
};
Rows = __decorate([
    inject('gameStore'),
    observer
], Rows);
export default Rows;
const breakpoints = [576, 768, 992, 1200, 1920];
const mq = facepaint(breakpoints.map(bp => `@media (min-width: ${bp}px)`));
const rowCss = css({
    position: 'relative',
    marginTop: ' 30px',
}, mq({
    height: [60, 80, 90, 110, 120, 150],
}));
const rowContainer = css `
	padding: 30px;
	width: 400px;
`;
