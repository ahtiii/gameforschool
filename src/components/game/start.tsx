import {Component, ReactNode} from "react";
import * as React from "react";
import Button from "../forms/button";
import {observer} from "mobx-react";
import {Link} from "react-router-dom";

@observer
export default class Start extends Component {
	public render(): ReactNode {
		return (
			<div>
				<Link to={'/type_input'}>
					<Button  label={'Start'}/>
				</Link>

				<Button label={'Beenden'} onClick={this.onClose}/>
			</div>
		);
	}

	public onClose = () => {
		window.close()
	}
}
