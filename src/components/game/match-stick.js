var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import * as React from 'react';
import { Component } from 'react';
import { css } from 'emotion';
import { inject, observer } from 'mobx-react';
import facepaint from 'facepaint';
let MatchStick = class MatchStick extends Component {
    render() {
        return (React.createElement("div", { className: styleling, onClick: this.props.onClick },
            React.createElement("div", { className: head })));
    }
};
MatchStick = __decorate([
    inject('player1', 'player2'),
    observer
], MatchStick);
export default MatchStick;
const breakpoints = [576, 768, 992, 1920];
const mq = facepaint(breakpoints.map(bp => `@media (min-width: ${bp}px)`));
const head = css({
    backgroundColor: 'darkred',
    height: 20,
    width: 15,
}, mq({
    width: [8, 10, 13, 15],
    height: [12, 16, 19, 20],
}));
const styleling = css({
    position: 'relative',
    display: 'inline-block',
    backgroundColor: 'wheat',
    boxShadow: '5px -3px #27ae60',
    color: 'white',
    cursor: 'pointer',
    margin: '0 20px',
    top: 0,
    '&:hover': {
        top: -7,
    },
}, mq({
    width: [8, 10, 13, 15],
    height: [44, 55, 72, 120],
}));
