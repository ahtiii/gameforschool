var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import GameStore from "../../stores/game-store";
import { Component } from "react";
import { inject, observer, Provider } from "mobx-react";
import { css } from "emotion";
import Rows from "./rows";
import Button from "../forms/button";
import * as React from "react";
import { computed, observable } from "mobx";
import { Link } from "react-router-dom";
let Game = class Game extends Component {
    constructor() {
        super(...arguments);
        this.gameStore = new GameStore(this.props.player1, [this.props.player1, this.props.player2], this.props.overlayStore);
        this.showContent = false;
        this.endTurn = () => {
            this.gameStore.endTurn();
        };
        this.onRestartClick = () => {
            this.gameStore.restartGame(1, 8, 2);
        };
    }
    render() {
        return (React.createElement("div", null,
            React.createElement(Provider, { gameStore: this.gameStore },
                React.createElement("div", { className: flex },
                    React.createElement("div", null,
                        this.gameStore.getSumRows ?
                            React.createElement(Rows, null) : (React.createElement("div", null,
                            React.createElement(Button, { onClick: this.onRestartClick, label: 'Restart' }),
                            React.createElement(Link, { to: '/type_input' },
                                React.createElement(Button, { label: 'Spieltyp Auswahl' })))),
                        this.gameStore.getSumRows === 0 && (React.createElement("div", null,
                            React.createElement("h1", null, this.gameStore.currentPlayer.getId == 1 ? this.gameStore.players[0].name + ' hat Gewonnen' : this.gameStore.players[1].name + ' hat Gewonnen')))),
                    React.createElement("div", { className: infoCss }, this.gameStore.getSumRows !== 0 && (React.createElement("div", null,
                        React.createElement("h2", null,
                            this.gameStore.getCurrentPlayerName,
                            " ist am Zug"),
                        React.createElement(Button, { onClick: this.endTurn, label: 'Runde beenden', disabled: this.isClickable }))))))));
    }
    componentDidMount() {
        if (this.props.player1) {
            if (this.props.player1._name) {
                this.showContent = true;
                return;
            }
        }
    }
    get isClickable() {
        return this.gameStore.matchstickCount == null;
    }
};
__decorate([
    observable.ref
], Game.prototype, "showContent", void 0);
__decorate([
    computed
], Game.prototype, "isClickable", null);
Game = __decorate([
    inject('player1', 'player2', 'overlayStore'),
    observer
], Game);
export default Game;
const flex = css `
	display: flex;
	justify-content: center;
`;
const infoCss = css `
	align-self: center;
	width: 300px;
`;
