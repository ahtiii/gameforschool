var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from "react";
import * as React from "react";
import Button from "../forms/button";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";
let Start = class Start extends Component {
    constructor() {
        super(...arguments);
        this.onClose = () => {
            window.close();
        };
    }
    render() {
        return (React.createElement("div", null,
            React.createElement(Link, { to: '/type_input' },
                React.createElement(Button, { label: 'Start' })),
            React.createElement(Button, { label: 'Beenden', onClick: this.onClose })));
    }
};
Start = __decorate([
    observer
], Start);
export default Start;
