import * as React from 'react';
import {Component} from 'react';
import {css} from 'emotion';
import {inject, observer} from 'mobx-react';
import PlayerStore from '../../stores/player-store';
import facepaint from 'facepaint';

export interface MatchStickProps {
	onClick?: () => void;
	player1?: PlayerStore;
	player2?: PlayerStore;
}

@inject('player1', 'player2')
@observer
export default class MatchStick extends Component<MatchStickProps> {
	public render() {
		return (
			<div className={styleling} onClick={this.props.onClick}>
				<div className={head}/>
			</div>
		);
	}
}
const breakpoints = [576, 768, 992, 1920];

const mq = facepaint(
	breakpoints.map(bp => `@media (min-width: ${bp}px)`),
);
const head = css({
		backgroundColor: 'darkred',
		height: 20,
		width: 15,
	},
	mq({
		width: [8, 10, 13, 15],
		height: [12, 16, 19, 20],
	}));

const styleling = css({

		position: 'relative',
		display: 'inline-block',
		backgroundColor: 'wheat',
		boxShadow: '5px -3px #27ae60',
		color: 'white',
		cursor: 'pointer',
		margin: '0 20px',
		top: 0,

		'&:hover': {
			top: -7,
		},
	},
	mq({
		width: [8, 10, 13, 15],
		height: [44, 55, 72, 120],
	}),
);


