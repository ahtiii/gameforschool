import {css, cx} from 'emotion';
import * as _ from 'lodash';
import MatchStick from './match-stick';
import * as React from 'react';
import {Component} from 'react';
import {inject, observer} from 'mobx-react';
import GameStore from '../../stores/game-store';
import facepaint from 'facepaint';

export interface RowsProps {
	gameStore?: GameStore;
}

@inject('gameStore')
@observer
export default class Rows extends Component<RowsProps> {
	public render() {
		return (
			<div  className={rowContainer}>
				{this.createMatchstickPyramid()}
			</div>
		);
	}

	public createMatchstickPyramid = () => {
		const {gameStore} = this.props;

		if (gameStore) {
			return gameStore.rowsWithCount!.map((row, rowkey) => {
				return (
					<div  className={cx(rowCss)} key={rowkey}>
						{_.range(row.count).map((stick, key) => {
							return (
								<MatchStick key={key} onClick={
									() => {
										if (!gameStore.notClickable) {
											gameStore.updateRow(rowkey);
											this.createMatchstickPyramid();
										}
									}
								}/>
							);
						})}
					</div>
				);
			});
		}

		return null;
	};
}

const breakpoints = [576, 768, 992, 1200, 1920];

const mq = facepaint(
	breakpoints.map(bp => `@media (min-width: ${bp}px)`),
);

const rowCss = css({
		position: 'relative',
		marginTop:' 30px',
	},
	mq({
		height: [60, 80, 90, 110, 120, 150],
	}),
);

const rowContainer = css`
	padding: 30px;
	width: 400px;
`;
