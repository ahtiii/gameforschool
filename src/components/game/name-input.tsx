import {Component} from "react";
import * as React from 'react';
import {inject, observer} from "mobx-react";
import PlayerStore from "../../stores/player-store";
import {Link} from "react-router-dom";
import Button from "../forms/button";
import {action, observable} from "mobx";
import Input from "../forms/input";
import {css} from "emotion";

export interface NameInputProps {
	player1?: PlayerStore;
	player2?: PlayerStore;
}

@inject('player1', 'player2')
@observer
export default class NameInput extends Component<NameInputProps> {
	@observable
	private player1Name: string | null = null;

	@observable
	private player2Name: string | null = null;

	public render() {
		return (
			<div className={nameInput}>
				<Input value={this.player1Name}
					   placeholder={'Player 1'}
					   onChange={action((value: string) => this.player1Name = value)}
					   className={''}/>
				{!this.props.player2!.isComputer &&
                < Input value={this.player2Name}
                        placeholder={'Player 2'}
                        onChange={action((value: string) => this.player2Name = value)}
                        className={''}/>
				}

				<Link to={'/game'}>
					<Button label={'Starte Spiel'} onClick={this.onClick}/>
				</Link>

				<Link to={'/type_input'}>
					<Button label={'Zurück'}/>
				</Link>
			</div>
		)
	}

	private onClick = () => {
		const {player1, player2} = this.props;

		if (player1!.isComputer) {
			player1!.setName('Computer')
		}

		if (player2!.isComputer) {
			player2!.setName('Computer')
		}

		if (!player1!.name) {
			this.player1Name ? player1!.setName(this.player1Name) : player1!.setName('Player 1');
		}
		if (!player2!.name) {
			this.player2Name ? player2!.setName(this.player2Name) : player2!.setName('Player 2');
		}
	}
}

const nameInput = css`
	align-items: center;
`;

