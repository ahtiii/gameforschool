var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from "react";
import * as React from 'react';
import { inject, observer } from "mobx-react";
import { Link } from "react-router-dom";
import Button from "../forms/button";
import { action, observable } from "mobx";
import Input from "../forms/input";
import { css } from "emotion";
let NameInput = class NameInput extends Component {
    constructor() {
        super(...arguments);
        this.player1Name = null;
        this.player2Name = null;
        this.onClick = () => {
            const { player1, player2 } = this.props;
            if (player1.isComputer) {
                player1.setName('Computer');
            }
            if (player2.isComputer) {
                player2.setName('Computer');
            }
            if (!player1.name) {
                this.player1Name ? player1.setName(this.player1Name) : player1.setName('Player 1');
            }
            if (!player2.name) {
                this.player2Name ? player2.setName(this.player2Name) : player2.setName('Player 2');
            }
        };
    }
    render() {
        return (React.createElement("div", { className: nameInput },
            React.createElement(Input, { value: this.player1Name, placeholder: 'Player 1', onChange: action((value) => this.player1Name = value), className: '' }),
            !this.props.player2.isComputer &&
                React.createElement(Input, { value: this.player2Name, placeholder: 'Player 2', onChange: action((value) => this.player2Name = value), className: '' }),
            React.createElement(Link, { to: '/game' },
                React.createElement(Button, { label: 'Starte Spiel', onClick: this.onClick })),
            React.createElement(Link, { to: '/type_input' },
                React.createElement(Button, { label: 'Zurück' }))));
    }
};
__decorate([
    observable
], NameInput.prototype, "player1Name", void 0);
__decorate([
    observable
], NameInput.prototype, "player2Name", void 0);
NameInput = __decorate([
    inject('player1', 'player2'),
    observer
], NameInput);
export default NameInput;
const nameInput = css `
	align-items: center;
`;
