import PlayerStore from "../../stores/player-store";
import GameStore from "../../stores/game-store";
import {Component, ReactNode} from "react";
import {inject, observer, Provider} from "mobx-react";
import {css} from "emotion";
import Rows from "./rows";
import Button from "../forms/button";
import * as React from "react";
import {RouteComponentProps} from "react-router";
import {computed, observable} from "mobx";
import OverlayStore from '../../stores/overlay-store';
import {Link} from "react-router-dom";

type PathParamsType = {
	param1?: string,
}

type PropsType = RouteComponentProps<PathParamsType> & {
	player1?: PlayerStore;
	player2?: PlayerStore;
	overlayStore?: OverlayStore
}


@inject('player1', 'player2', 'overlayStore')
@observer
export default class Game extends Component<PropsType> {

	private gameStore: GameStore = new GameStore(
		this.props.player1!,
		[this.props.player1!, this.props.player2!],
		this.props.overlayStore!);

	@observable.ref
	private showContent: boolean = false;

	public render(): ReactNode {
		return (
			<div>
				<Provider gameStore={this.gameStore}>
					<div className={flex}>
						<div>
							{this.gameStore.getSumRows ?
								<Rows/> : (
								<div>
									<Button onClick={this.onRestartClick} label={'Restart'}/>
									<Link to={'/type_input'}>
										<Button label={'Spieltyp Auswahl'}/>
									</Link>
								</div>
							)}

							{this.gameStore.getSumRows === 0 && (
								<div>
									<h1>{this.gameStore.currentPlayer.getId == 1 ? this.gameStore.players[0].name + ' hat Gewonnen' : this.gameStore.players[1].name + ' hat Gewonnen'}</h1>
								</div>
							)}
						</div>

						<div className={infoCss}>
							{this.gameStore.getSumRows !== 0 && (
								<div>
									<h2>{this.gameStore.getCurrentPlayerName} ist am Zug</h2>

									<Button onClick={this.endTurn}
											label={'Runde beenden'}
											disabled={this.isClickable}/>
								</div>
							)}
						</div>
					</div>
				</Provider>
			</div>
		);
	}

	private endTurn = () => {
		this.gameStore.endTurn()
	};

	private onRestartClick = () => {
		this.gameStore.restartGame(1, 8, 2);
	};

	componentDidMount() {
		if (this.props.player1) {
			if (this.props.player1._name) {
				this.showContent = true;
				return;
			}
		}
	}

	@computed get isClickable() {
		return this.gameStore.matchstickCount == null
	}
}

const flex = css`
	display: flex;
	justify-content: center;
`;

const infoCss = css`
	align-self: center;
	width: 300px;
`;

