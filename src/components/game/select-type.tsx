import {Component} from "react";
import *as React from "react";
import Button from "../forms/button";
import {Link} from "react-router-dom";
import {inject, observer} from "mobx-react";
import PlayerStore from "../../stores/player-store";
import {css} from "emotion";

export interface SelectTypeProps {
	player1?: PlayerStore;
	player2?: PlayerStore;
}

@inject('player1', 'player2')
@observer
export default class SelectType extends Component<SelectTypeProps> {

	public render(): React.ReactNode {
		return (
			<div className={typeSelect}>
				<Link to={'/name_input'}>
					<Button onClick={this.setToOnePlayer} label={'Ein Spieler'}/>
				</Link>

				<Link to={'/name_input'}>
					<Button onClick={this.setToTwoPlayer} label={'Zwei Spieler'}/>
				</Link>
			</div>
		)
	}

	private setToOnePlayer = () => {
		this.props.player2!.setcomputer(true)
	};

	private setToTwoPlayer = () => {
		this.props.player2!.setName(null);
		this.props.player2!.setcomputer(false);
		this.props.player1!.setcomputer(false);
	};
}
const typeSelect = css`
	vertical-align: middle;
	display: table-cell;
`;


