var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from "react";
import * as React from "react";
import Button from "../forms/button";
import { Link } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { css } from "emotion";
let SelectType = class SelectType extends Component {
    constructor() {
        super(...arguments);
        this.setToOnePlayer = () => {
            this.props.player2.setcomputer(true);
        };
        this.setToTwoPlayer = () => {
            this.props.player2.setName(null);
            this.props.player2.setcomputer(false);
            this.props.player1.setcomputer(false);
        };
    }
    render() {
        return (React.createElement("div", { className: typeSelect },
            React.createElement(Link, { to: '/name_input' },
                React.createElement(Button, { onClick: this.setToOnePlayer, label: 'Ein Spieler' })),
            React.createElement(Link, { to: '/name_input' },
                React.createElement(Button, { onClick: this.setToTwoPlayer, label: 'Zwei Spieler' }))));
    }
};
SelectType = __decorate([
    inject('player1', 'player2'),
    observer
], SelectType);
export default SelectType;
const typeSelect = css `
	vertical-align: middle;
	display: table-cell;
`;
