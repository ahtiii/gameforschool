import * as React from 'react';

import {observer} from "mobx-react";
import {css, cx} from "emotion";

export interface InputProps {
	type?: string;
	className?: string;
	value?: string | null;
	placeholder?: string;
	onChange: (inputText: string) => void;
	label?: string;
}

const inputStyle = css`
	width: 350px;
	border: 3px solid white;
	border-radius: 4px;
	height: 25px;
	padding: 2px 2px 2px 2px;
	&:focus {
		outline:none;
		border: 2px solid #cc2e3a;
	}
`;

const group = css`
	//display: flex;
	//justify-content: center;
	//align-items: center;
	position: relative;
	margin-bottom:20px;
`;

@observer
export default class Input extends React.Component<InputProps> {

	render() {
		const {placeholder, type, value, className} = this.props;

		return (
			<div className={group}>
				<input
					placeholder={placeholder}
					type={type}
					className={cx(className, inputStyle)}
					value={value || ''}
					onChange={this.onChange}
				/>
			</div>
		);


	}

	onChange = (event: React.FocusEvent<HTMLInputElement>) => {
		this.props.onChange(event.target.value);

	}
}

