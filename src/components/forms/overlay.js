var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from "react";
import * as React from 'react';
import { observer } from "mobx-react";
import { css } from "emotion";
import { faTimes } from "@fortawesome/free-solid-svg-icons/faTimes";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import posed, { PoseGroup } from "react-pose";
import { observable } from "mobx";
const FadeInOverlay = posed.div({
    enter: {
        opacity: 1.0,
        transition: {
            duration: 400,
        }
    },
    exit: {
        opacity: 0.0,
        transition: {
            duration: 400
        }
    }
});
let Overlay = class Overlay extends Component {
    constructor() {
        super(...arguments);
        this.isVisable = false;
        this.onClose = () => {
            this.isVisable = false;
            setTimeout(() => {
                this.props.onClose();
            }, 2000);
            if (this.props.options.onClick) {
                this.props.options.onClick();
            }
        };
    }
    render() {
        return (React.createElement(PoseGroup, null, this.isVisable && (React.createElement(FadeInOverlay, { className: overlayBase, key: this.props.id },
            React.createElement("div", { className: overlay },
                React.createElement("div", { className: cell },
                    React.createElement("a", { className: message }, this.props.renderOverlayContent)),
                React.createElement("div", { onClick: this.onClose, className: closeButton },
                    React.createElement(FontAwesomeIcon, { size: "2x", icon: faTimes })))))));
    }
    componentDidMount() {
        this.isVisable = true;
        if (this.props.options.timeout) {
            setTimeout(() => this.isVisable = false, 2000);
        }
    }
};
__decorate([
    observable
], Overlay.prototype, "isVisable", void 0);
Overlay = __decorate([
    observer
], Overlay);
export default Overlay;
const overlayBase = css `
	position: absolute;
	top: auto;
	right: auto;
	bottom: 50%;
	left:50%;
	transform: translate(-50%, 0%);
	z-index: 1;
`;
const overlay = css `
	display: flex;
	background-color: white;
	border: 2px solid darkslategrey;
	box-shadow: 5px -3px #7b7f7d73;
	border-radius: 8px;
	width: auto; 
	`;
const cell = css `
	display: inline-block;
	margin: auto;
	padding-left: 20px;
`;
const message = css `
	position: sticky;
	font-size: 20px;
	color: darkred;
	font-family: Fahkwang;
	font-weight: 600;
	line-height: normal;
	padding-right: 10px;
	`;
const closeButton = css `
	margin-left: 20px;
	border: none;
	background-color: transparent;
	cursor: pointer;
	border-left: 2px solid gray;
	color: black;
	padding:20px;

	&:hover {
		background-color: #cc2e3a;
		color: #f7dbdd;
		border-radius: 8px;
	}
`;
