var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import * as React from 'react';
import { observer } from "mobx-react";
import { css, cx } from "emotion";
const inputStyle = css `
	width: 350px;
	border: 3px solid white;
	border-radius: 4px;
	height: 25px;
	padding: 2px 2px 2px 2px;
	&:focus {
		outline:none;
		border: 2px solid #cc2e3a;
	}
`;
const group = css `
	//display: flex;
	//justify-content: center;
	//align-items: center;
	position: relative;
	margin-bottom:20px;
`;
let Input = class Input extends React.Component {
    constructor() {
        super(...arguments);
        this.onChange = (event) => {
            this.props.onChange(event.target.value);
        };
    }
    render() {
        const { placeholder, type, value, className } = this.props;
        return (React.createElement("div", { className: group },
            React.createElement("input", { placeholder: placeholder, type: type, className: cx(className, inputStyle), value: value || '', onChange: this.onChange })));
    }
};
Input = __decorate([
    observer
], Input);
export default Input;
