import {Component, ReactNode} from "react";
import * as React from 'react';
import {OverlayProps} from "../../stores/overlay-store";
import {observer} from "mobx-react";
import {css} from "emotion";
import {faTimes} from "@fortawesome/free-solid-svg-icons/faTimes";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import posed, {PoseGroup} from "react-pose";
import {observable} from "mobx";

const FadeInOverlay = posed.div({
	enter: {
		opacity: 1.0,
		transition: {
			duration: 400,
		}
	},
	exit: {
		opacity: 0.0,
		transition: {
			duration: 400
		}
	}
});

@observer
export default class Overlay extends Component<OverlayProps> {

	@observable
	private isVisable: boolean = false;

	public render(): ReactNode {
		return (
			<PoseGroup>
				{this.isVisable && (
					<FadeInOverlay className={overlayBase} key={this.props.id}>
						<div className={overlay}>
							<div className={cell}>
								<a className={message}>{this.props.renderOverlayContent}</a>
							</div>
							<div onClick={this.onClose} className={closeButton}>
								<FontAwesomeIcon size={"2x"} icon={faTimes}/>
							</div>
						</div>
					</FadeInOverlay>
				)}
			</PoseGroup>
		)
	}

	private onClose = () => {
		this.isVisable = false;
		setTimeout(() => {
			this.props.onClose()
		}, 2000);

		if (this.props.options.onClick) {
			this.props.options.onClick()
		}
	};

	public componentDidMount() {
		this.isVisable = true;
		if (this.props.options.timeout) {
			setTimeout(() => this.isVisable = false, 2000)
		}
	}
}

const overlayBase = css`
	position: absolute;
	top: auto;
	right: auto;
	bottom: 50%;
	left:50%;
	transform: translate(-50%, 0%);
	z-index: 1;
`;

const overlay = css`
	display: flex;
	background-color: white;
	border: 2px solid darkslategrey;
	box-shadow: 5px -3px #7b7f7d73;
	border-radius: 8px;
	width: auto; 
	`;

const cell = css`
	display: inline-block;
	margin: auto;
	padding-left: 20px;
`;

const message = css`
	position: sticky;
	font-size: 20px;
	color: darkred;
	font-family: Fahkwang;
	font-weight: 600;
	line-height: normal;
	padding-right: 10px;
	`;

const closeButton = css`
	margin-left: 20px;
	border: none;
	background-color: transparent;
	cursor: pointer;
	border-left: 2px solid gray;
	color: black;
	padding:20px;

	&:hover {
		background-color: #cc2e3a;
		color: #f7dbdd;
		border-radius: 8px;
	}
`;
