import * as React from 'react'
import {Component, ReactNode} from "react";
import {css, cx} from "emotion";


export interface ButtonProps {
	label?: string
	className?: string;
	disabled?: boolean;
	onClick?: () => void;
}

const buttonCss = css`
	border-radius: 4px;
	color: #f7dbdd;
	padding: 16px 32px;
	font-size: 20px;
	cursor: pointer;
	transition-duration: 0.4s;
	font-weight:600;
	margin: 10px 10px 10px 10px;

`;

const enabledCss = css`
	background-color:#cc2e3a ;
	border: 2px solid #cc2e3a;
	&:hover{
		background-color: #f7dbdd;
		color: #cc2e3a;
		font-size: 20px;
		font-weight: 600;
}
`;

const disabledCss = css`
	background-color: #5f5f5f ;
	border: 2px solid #5f5f5f;
`;

export default class Button extends Component <ButtonProps> {
	public render(): ReactNode {
		const {onClick, label, className, disabled} = this.props;
		return (
			<button
				disabled={disabled}
				className={cx({[enabledCss]: !disabled}, {[disabledCss]: disabled}, className, buttonCss)}
				onClick={onClick}>
				{label}
			</button>
		)
	}
}