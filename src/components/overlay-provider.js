var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from "react";
import * as React from "react";
import { observer, Provider } from "mobx-react";
import { css } from "emotion";
import OverlayRenderer from "./overlay-renderer";
import OverlayStore from "../stores/overlay-store";
const className = css `
	height: 100%;
	width: 100%;
`;
let OverlayProvider = class OverlayProvider extends Component {
    constructor() {
        super(...arguments);
        this.overlayStore = new OverlayStore();
    }
    render() {
        return (React.createElement(Provider, { overlayStore: this.overlayStore },
            React.createElement("div", { className: className },
                React.createElement(OverlayRenderer, { store: this.overlayStore }),
                this.props.children)));
    }
};
OverlayProvider = __decorate([
    observer
], OverlayProvider);
export default OverlayProvider;
