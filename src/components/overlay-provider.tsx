import {Component, ReactNode} from "react";
import * as React from "react";
import {observer, Provider} from "mobx-react";
import {css} from "emotion";
import OverlayRenderer from "./overlay-renderer";
import OverlayStore from "../stores/overlay-store";

const className = css`
	height: 100%;
	width: 100%;
`;

@observer
export default class OverlayProvider extends Component {
	public overlayStore: OverlayStore = new OverlayStore();

	public render(): ReactNode {
		return (
			<Provider overlayStore={this.overlayStore}>
				<div className={className}>
					<OverlayRenderer store={this.overlayStore}/>
					{this.props.children}
				</div>
			</Provider>
		)
	}
}
