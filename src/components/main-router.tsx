import * as React from 'react';
import {Component, ReactNode} from 'react';
import {inject, observer} from 'mobx-react';
import {Redirect, Route, Switch} from "react-router-dom";
import Start from "./game/start";
import Game from "./game/game";
import NameInput from "./game/name-input";
import PlayerStore from "../stores/player-store";
import SelectType from "./game/select-type";


export interface MainProps {
	player1?: PlayerStore;
	player2?: PlayerStore;
}

@inject('player1', 'player2')
@observer
export default class MainRouter extends Component<MainProps> {

	public render(): ReactNode {
		return (
			<Switch>
				<Route exact path={'/'} component={Start}/>
				<Route exact path={'/type_input'} component={SelectType}/>
				<Route exact path={'/name_input'} component={NameInput}/>
				<PrivateRoute path={'/game'} player1={this.props.player1!} component={Game}/>
				<Redirect to={'/'} key={'redirect'}/>
			</Switch>
		)
	}
}
const PrivateRoute = (
	{component: Component, path: path, player1: player1, ...rest}:
		{ component: any, path: any, player1: PlayerStore }
) => (
	<Route {...rest} render={(props) => (
		player1._name
			? <Component {...props} />
			: <Redirect to={'/'}/>
	)}/>);

