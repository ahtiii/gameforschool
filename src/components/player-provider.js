var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from "react";
import * as React from "react";
import { observer, Provider } from "mobx-react";
import MainRouter from "./main-router";
import PlayerStore from "../stores/player-store";
import { css } from "emotion";
const className = css `
	text-align: center;
	position: absolute;
	display: flex;
	justify-content: center;
	align-items: center;
	height: 100%;
	width: 100%;
	top:0;
	left:0;
	`;
let PlayerProvider = class PlayerProvider extends Component {
    constructor() {
        super(...arguments);
        this.player1 = new PlayerStore(1);
        this.player2 = new PlayerStore(2);
    }
    render() {
        return (React.createElement(Provider, { player1: this.player1, player2: this.player2 },
            React.createElement("div", { className: className },
                React.createElement(MainRouter, null))));
    }
};
PlayerProvider = __decorate([
    observer
], PlayerProvider);
export default PlayerProvider;
