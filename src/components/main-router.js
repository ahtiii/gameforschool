var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
import * as React from 'react';
import { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Redirect, Route, Switch } from "react-router-dom";
import Start from "./game/start";
import Game from "./game/game";
import NameInput from "./game/name-input";
import SelectType from "./game/select-type";
let MainRouter = class MainRouter extends Component {
    render() {
        return (React.createElement(Switch, null,
            React.createElement(Route, { exact: true, path: '/', component: Start }),
            React.createElement(Route, { exact: true, path: '/type_input', component: SelectType }),
            React.createElement(Route, { exact: true, path: '/name_input', component: NameInput }),
            React.createElement(PrivateRoute, { path: '/game', player1: this.props.player1, component: Game }),
            React.createElement(Redirect, { to: '/', key: 'redirect' })));
    }
};
MainRouter = __decorate([
    inject('player1', 'player2'),
    observer
], MainRouter);
export default MainRouter;
const PrivateRoute = (_a) => {
    var { component: Component, path: path, player1: player1 } = _a, rest = __rest(_a, ["component", "path", "player1"]);
    return (React.createElement(Route, Object.assign({}, rest, { render: (props) => (player1._name
            ? React.createElement(Component, Object.assign({}, props))
            : React.createElement(Redirect, { to: '/' })) })));
};
