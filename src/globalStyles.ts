import {injectGlobal} from 'emotion';

injectGlobal`
    body {
    background-color: #2ecc71;
    font-family:'Fahkwang';
    }
    /* fahkwang-300 - latin_latin-ext */
@font-face {
  font-family: 'Fahkwang';
  font-style: normal;
  font-weight: 300;
  src: url('../shared/fonts/fahkwang-v1-latin_latin-ext-300.eot'); /* IE9 Compat Modes */
  src: local('Fahkwang Light'), local('Fahkwang-Light'),
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-300.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-300.woff2') format('woff2'), /* Super Modern Browsers */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-300.woff') format('woff'), /* Modern Browsers */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-300.ttf') format('truetype'), /* Safari, Android, iOS */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-300.svg#Fahkwang') format('svg'); /* Legacy iOS */
}

/* fahkwang-regular - latin_latin-ext */
@font-face {
  font-family: 'Fahkwang';
  font-style: normal;
  font-weight: 400;
  src: url('../shared/fonts/fahkwang-v1-latin_latin-ext-regular.eot'); /* IE9 Compat Modes */
  src: local('Fahkwang Regular'), local('Fahkwang-Regular'),
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-regular.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-regular.woff2') format('woff2'), /* Super Modern Browsers */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-regular.woff') format('woff'), /* Modern Browsers */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-regular.ttf') format('truetype'), /* Safari, Android, iOS */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-regular.svg#Fahkwang') format('svg'); /* Legacy iOS */
}

/* fahkwang-500 - latin_latin-ext */
@font-face {
  font-family: 'Fahkwang';
  font-style: normal;
  font-weight: 500;
  src: url('../shared/fonts/fahkwang-v1-latin_latin-ext-500.eot'); /* IE9 Compat Modes */
  src: local('Fahkwang Medium'), local('Fahkwang-Medium'),
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-500.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-500.woff2') format('woff2'), /* Super Modern Browsers */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-500.woff') format('woff'), /* Modern Browsers */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-500.ttf') format('truetype'), /* Safari, Android, iOS */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-500.svg#Fahkwang') format('svg'); /* Legacy iOS */
}

/* fahkwang-600 - latin_latin-ext */
@font-face {
  font-family: 'Fahkwang';
  font-style: normal;
  font-weight: 600;
  src: url('../shared/fonts/fahkwang-v1-latin_latin-ext-600.eot'); /* IE9 Compat Modes */
  src: local('Fahkwang SemiBold'), local('Fahkwang-SemiBold'),
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-600.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-600.woff2') format('woff2'), /* Super Modern Browsers */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-600.woff') format('woff'), /* Modern Browsers */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-600.ttf') format('truetype'), /* Safari, Android, iOS */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-600.svg#Fahkwang') format('svg'); /* Legacy iOS */
}

/* fahkwang-700 - latin_latin-ext */
@font-face {
  font-family: 'Fahkwang';
  font-style: normal;
  font-weight: 700;
  src: url('../shared/fonts/fahkwang-v1-latin_latin-ext-700.eot'); /* IE9 Compat Modes */
  src: local('Fahkwang Bold'), local('Fahkwang-Bold'),
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-700.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-700.woff2') format('woff2'), /* Super Modern Browsers */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-700.woff') format('woff'), /* Modern Browsers */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-700.ttf') format('truetype'), /* Safari, Android, iOS */
       url('../shared/fonts/fahkwang-v1-latin_latin-ext-700.svg#Fahkwang') format('svg'); /* Legacy iOS */
}

`;
