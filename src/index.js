import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './globalStyles';
import { BrowserRouter } from "react-router-dom";
import OverlayProvider from "./components/overlay-provider";
import PlayerProvider from "./components/player-provider";
const Index = () => {
    return (React.createElement(BrowserRouter, null,
        React.createElement(OverlayProvider, null,
            React.createElement(PlayerProvider, null))));
};
ReactDOM.render(React.createElement(Index, null), document.getElementById('index'));
