import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './globalStyles'
import {BrowserRouter} from "react-router-dom";
import OverlayProvider from "./components/overlay-provider";
import PlayerProvider from "./components/player-provider";

const Index = () => {
	return (
		<BrowserRouter>
			<OverlayProvider>
				<PlayerProvider/>
			</OverlayProvider>
		</BrowserRouter>
	);
};

ReactDOM.render(
	<Index/>,
	document.getElementById('index'));
