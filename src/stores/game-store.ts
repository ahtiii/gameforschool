import {computed, observable} from 'mobx';
import {range, reduce, sumBy} from 'lodash';
import PlayerStore from './player-store';
import OverlayStore from './overlay-store';

export default class GameStore {
	constructor(player: PlayerStore, players: PlayerStore[], overlayStore: OverlayStore) {
		this.currentPlayer = player;
		this.players = players;
		this.overlayStore = overlayStore;
	}

	@observable
	overlayStore: OverlayStore;

	@observable
	selectedRow: number | null = null;

	@observable
	matchstickCount: number | null = null;

	@observable
	players: PlayerStore[] = [];

	@observable
	currentPlayer: PlayerStore;

	@observable
	_rows: number[] = range(1, 8, 2);

	@observable
	notClickable: boolean = false;

	@observable
	wrongRow: boolean = false;

	@computed get rows(): number[] {
		return this._rows;
	}

	@computed get rowsWithCount(): Rows[] {
		return this._rows.map((value) => {
			return {count: value};
		});
	}

	@computed get getCurrentPlayerName() {
		return this.currentPlayer._name;
	}

	@computed get getMatchStickCount() {
		return this.matchstickCount;
	}

	@computed get getStateText() {
		return this.wrongRow;
	}

	@computed get getSumRows() {
		return sumBy(this.rowsWithCount, 'count')
	}

	setRows(rows: number[]) {
		this._rows = rows;
	}

	modulo4(number: number): number {
		return number % 4;
	}

	restartGame(start: number, end: number, step: number) {
		this.notClickable = false;
		this.wrongRow = false;
		this.matchstickCount = null;
		this.selectedRow = null;
		this.currentPlayer = this.players[Math.floor(Math.random() * 2)];
		this.setRows(range(start, end, step));

		if (this.currentPlayer.isComputer) {
			this.overlayStore.addOverlay(this.currentPlayer.name + ' berechnet seinen Zug !  ', {
				timeout: 2000
			});
			setTimeout(this.pcTurn, 2000)
		}
	}

	setCurrentPlayer(player: PlayerStore) {
		this.currentPlayer = player;
	}

	changeCurrentPlayer() {
		this.notClickable = true;
		this.currentPlayer._id == 1 ? this.setCurrentPlayer(this.players[1]) : this.setCurrentPlayer(this.players[0]);

		if (this.currentPlayer.isComputer) {
			this.overlayStore.addOverlay(this.currentPlayer.name + ' berechnet seinen Zug !  ', {
				timeout: 2000
			});
			setTimeout(this.pcTurn, 2000)
		} else {
			this.overlayStore.addOverlay(this.currentPlayer.name + ' ist daran !', {
				timeout: 2000, onClick: () => {
					this.notClickable = false
				}
			});
		}
	}

	endTurn() {
		this.wrongRow = false;
		this.matchstickCount = null;
		this.selectedRow = null;
		this.changeCurrentPlayer();
		return;
	}

	options() {
		let sum: number = reduce(this.rows, (sum, row) => {
			//Modulo 4 wegen 3 Hölzern
			return sum ^ this.modulo4(row);
		}, 0);
		const options: MatchStickOptions[] = [];
		if (sum) {
			this.rows.map((row, key) => {
				let mod = this.modulo4(row);
				let check = sum ^ mod;
				if (row > check && mod != check) {
					options.push({rowNumber: key, matchstickNumber: this.modulo4(row - check)});
				}
			})
		}
		return options
	}

	randomRowNumber = (number: number): number => {
		return Math.floor(Math.random() * number)
	};

	randomNumberRange = (max: number, min: number) => {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	};

	pcTurn = () => {
		this.notClickable = true;
		let options = this.options();

		if (options.length > 0) {
			console.log(options);
			let randomRowNumber = this.randomRowNumber(options.length);
			let option = options[randomRowNumber];

			this.rows[option.rowNumber] -= option.matchstickNumber;
		} else {
			let row = this.randomRowNumber(this.rows.length);
			console.log(this.rows);
			while (0 == this.rows[row]) {
				row = this.randomRowNumber(this.rows.length)
			}

			this.rows[row] -= this.randomNumberRange(1, 3);
		}

		if (this.getSumRows) {
			setTimeout(() => {
				this.endTurn()
			}, 200);
		}
	};

	updateRow(row: number) {
		if (this.selectedRow == null) {
			this.selectedRow = row;
		}

		if ((row === this.selectedRow) && this.matchstickCount! < 3) {
			this.rows[row]--;
			this.wrongRow = false;

			if (this.getSumRows === 0) {
				return;
			}
			this.matchstickCount!++;

			if (this.matchstickCount! > 2 || this.rows[this.selectedRow] === 0) {
				this.endTurn();
			}
			return;
		}

		if (this.selectedRow !== row) {
			this.wrongRow = true;
			this.overlayStore.addOverlay('Du kannst nicht von mehr als einer Reihe nehmen!!!👆  ', {timeout: 100000});
			return;
		}
	}
}

export interface Rows {
	count: number;
}

export interface MatchStickOptions {
	rowNumber: number;
	matchstickNumber: number;
}

