var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { computed, observable } from 'mobx';
import { range, reduce, sumBy } from 'lodash';
export default class GameStore {
    constructor(player, players, overlayStore) {
        this.selectedRow = null;
        this.matchstickCount = null;
        this.players = [];
        this._rows = range(1, 8, 2);
        this.notClickable = false;
        this.wrongRow = false;
        this.randomRowNumber = (number) => {
            return Math.floor(Math.random() * number);
        };
        this.randomNumberRange = (max, min) => {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        this.pcTurn = () => {
            this.notClickable = true;
            let options = this.options();
            if (options.length > 0) {
                console.log(options);
                let randomRowNumber = this.randomRowNumber(options.length);
                let option = options[randomRowNumber];
                this.rows[option.rowNumber] -= option.matchstickNumber;
            }
            else {
                let row = this.randomRowNumber(this.rows.length);
                console.log(this.rows);
                while (0 == this.rows[row]) {
                    row = this.randomRowNumber(this.rows.length);
                }
                this.rows[row] -= this.randomNumberRange(1, 3);
            }
            if (this.getSumRows) {
                setTimeout(() => {
                    this.endTurn();
                }, 200);
            }
        };
        this.currentPlayer = player;
        this.players = players;
        this.overlayStore = overlayStore;
    }
    get rows() {
        return this._rows;
    }
    get rowsWithCount() {
        return this._rows.map((value) => {
            return { count: value };
        });
    }
    get getCurrentPlayerName() {
        return this.currentPlayer._name;
    }
    get getMatchStickCount() {
        return this.matchstickCount;
    }
    get getStateText() {
        return this.wrongRow;
    }
    get getSumRows() {
        return sumBy(this.rowsWithCount, 'count');
    }
    setRows(rows) {
        this._rows = rows;
    }
    modulo4(number) {
        return number % 4;
    }
    restartGame(start, end, step) {
        this.notClickable = false;
        this.wrongRow = false;
        this.matchstickCount = null;
        this.selectedRow = null;
        this.currentPlayer = this.players[Math.floor(Math.random() * 2)];
        this.setRows(range(start, end, step));
        if (this.currentPlayer.isComputer) {
            this.overlayStore.addOverlay(this.currentPlayer.name + ' berechnet seinen Zug !  ', {
                timeout: 2000
            });
            setTimeout(this.pcTurn, 2000);
        }
    }
    setCurrentPlayer(player) {
        this.currentPlayer = player;
    }
    changeCurrentPlayer() {
        this.notClickable = true;
        this.currentPlayer._id == 1 ? this.setCurrentPlayer(this.players[1]) : this.setCurrentPlayer(this.players[0]);
        if (this.currentPlayer.isComputer) {
            this.overlayStore.addOverlay(this.currentPlayer.name + ' berechnet seinen Zug !  ', {
                timeout: 2000
            });
            setTimeout(this.pcTurn, 2000);
        }
        else {
            this.overlayStore.addOverlay(this.currentPlayer.name + ' ist daran !', {
                timeout: 2000, onClick: () => {
                    this.notClickable = false;
                }
            });
        }
    }
    endTurn() {
        this.wrongRow = false;
        this.matchstickCount = null;
        this.selectedRow = null;
        this.changeCurrentPlayer();
        return;
    }
    options() {
        let sum = reduce(this.rows, (sum, row) => {
            //Modulo 4 wegen 3 Hölzern
            return sum ^ this.modulo4(row);
        }, 0);
        const options = [];
        if (sum) {
            this.rows.map((row, key) => {
                let mod = this.modulo4(row);
                let check = sum ^ mod;
                if (row > check && mod != check) {
                    options.push({ rowNumber: key, matchstickNumber: this.modulo4(row - check) });
                }
            });
        }
        return options;
    }
    updateRow(row) {
        if (this.selectedRow == null) {
            this.selectedRow = row;
        }
        if ((row === this.selectedRow) && this.matchstickCount < 3) {
            this.rows[row]--;
            this.wrongRow = false;
            if (this.getSumRows === 0) {
                return;
            }
            this.matchstickCount++;
            if (this.matchstickCount > 2 || this.rows[this.selectedRow] === 0) {
                this.endTurn();
            }
            return;
        }
        if (this.selectedRow !== row) {
            this.wrongRow = true;
            this.overlayStore.addOverlay('Du kannst nicht von mehr als einer Reihe nehmen!!!👆  ', { timeout: 100000 });
            return;
        }
    }
}
__decorate([
    observable
], GameStore.prototype, "overlayStore", void 0);
__decorate([
    observable
], GameStore.prototype, "selectedRow", void 0);
__decorate([
    observable
], GameStore.prototype, "matchstickCount", void 0);
__decorate([
    observable
], GameStore.prototype, "players", void 0);
__decorate([
    observable
], GameStore.prototype, "currentPlayer", void 0);
__decorate([
    observable
], GameStore.prototype, "_rows", void 0);
__decorate([
    observable
], GameStore.prototype, "notClickable", void 0);
__decorate([
    observable
], GameStore.prototype, "wrongRow", void 0);
__decorate([
    computed
], GameStore.prototype, "rows", null);
__decorate([
    computed
], GameStore.prototype, "rowsWithCount", null);
__decorate([
    computed
], GameStore.prototype, "getCurrentPlayerName", null);
__decorate([
    computed
], GameStore.prototype, "getMatchStickCount", null);
__decorate([
    computed
], GameStore.prototype, "getStateText", null);
__decorate([
    computed
], GameStore.prototype, "getSumRows", null);
