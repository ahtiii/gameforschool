import {action, computed, observable} from "mobx";

export interface OverlayProps {
	id?: number | string;
	options: OverlayOptions
	renderOverlayContent: any;
	onClose?: any;
}

export interface OverlayOptions {
	type?: string;
	timeout?: number;
	onClick?: () => void;
}

export default class OverlayStore {
	@observable
	overlays: OverlayProps[] = [];

	@computed get allOverlays() {
		return this.overlays
	}

	@observable
	timerId: any[] = [];

	@action addOverlay(renderOverlayContent: any, options: OverlayOptions) {
		const id = Math.round(window.performance.now());
		let type: string | null = 'info';
		let timeout: number = 0;

		const overlayOptions: OverlayOptions = {
			timeout,
			type,
			...options
		};

		let onClose = () => this.disposeOverlay(alert.id);

		const alert = {
			id,
			onClose,
			renderOverlayContent,
			options: overlayOptions,
		};


		if (alert.options.timeout) {
			const timerId = setTimeout(() => {
				this.disposeOverlay(alert.id);

				this.timerId.splice(this.timerId.indexOf(timerId), 1)

			}, alert.options.timeout);
			this.timerId.push(timerId);
		}

		this.overlays.push(alert)
	}

	@action disposeOverlay(id: number | string) {
		this.overlays = this.overlays.filter((overlay) => {
			if (overlay.id == id) {
				if(overlay.options.onClick)
				{
					overlay.options.onClick()
				}
				let index= this.overlays.indexOf(overlay);
				this.overlays.splice(index,1);
				return false;
			}

			return true;
		});
	}
}