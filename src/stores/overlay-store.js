var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { action, computed, observable } from "mobx";
export default class OverlayStore {
    constructor() {
        this.overlays = [];
        this.timerId = [];
    }
    get allOverlays() {
        return this.overlays;
    }
    addOverlay(renderOverlayContent, options) {
        const id = Math.round(window.performance.now());
        let type = 'info';
        let timeout = 0;
        const overlayOptions = Object.assign({ timeout,
            type }, options);
        let onClose = () => this.disposeOverlay(alert.id);
        const alert = {
            id,
            onClose,
            renderOverlayContent,
            options: overlayOptions,
        };
        if (alert.options.timeout) {
            const timerId = setTimeout(() => {
                this.disposeOverlay(alert.id);
                this.timerId.splice(this.timerId.indexOf(timerId), 1);
            }, alert.options.timeout);
            this.timerId.push(timerId);
        }
        this.overlays.push(alert);
    }
    disposeOverlay(id) {
        this.overlays = this.overlays.filter((overlay) => {
            if (overlay.id == id) {
                if (overlay.options.onClick) {
                    overlay.options.onClick();
                }
                let index = this.overlays.indexOf(overlay);
                this.overlays.splice(index, 1);
                return false;
            }
            return true;
        });
    }
}
__decorate([
    observable
], OverlayStore.prototype, "overlays", void 0);
__decorate([
    computed
], OverlayStore.prototype, "allOverlays", null);
__decorate([
    observable
], OverlayStore.prototype, "timerId", void 0);
__decorate([
    action
], OverlayStore.prototype, "addOverlay", null);
__decorate([
    action
], OverlayStore.prototype, "disposeOverlay", null);
