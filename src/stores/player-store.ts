import {computed, observable} from "mobx";

export default class PlayerStore {
	constructor(id: number) {
		this._id = id
	}

	@observable.ref
	_name: string | null = null;

	@observable.ref
	_id: number = 0;

	@observable.ref
	_isComputer: boolean = false;

	@computed get name() {
		return this._name;
	}

	@computed get getId() {
		return this._id;
	}

	@computed get isComputer(){
		return this._isComputer;
	}

	setId(id: number) {
		this._id = id;
	}

	setcomputer(withComputer: boolean) {
		this._isComputer = withComputer;
	}

	setName(name: string | null) {
		this._name = name;
	}
}