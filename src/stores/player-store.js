var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { computed, observable } from "mobx";
export default class PlayerStore {
    constructor(id) {
        this._name = null;
        this._id = 0;
        this._isComputer = false;
        this._id = id;
    }
    get name() {
        return this._name;
    }
    get getId() {
        return this._id;
    }
    get isComputer() {
        return this._isComputer;
    }
    setId(id) {
        this._id = id;
    }
    setcomputer(withComputer) {
        this._isComputer = withComputer;
    }
    setName(name) {
        this._name = name;
    }
}
__decorate([
    observable.ref
], PlayerStore.prototype, "_name", void 0);
__decorate([
    observable.ref
], PlayerStore.prototype, "_id", void 0);
__decorate([
    observable.ref
], PlayerStore.prototype, "_isComputer", void 0);
__decorate([
    computed
], PlayerStore.prototype, "name", null);
__decorate([
    computed
], PlayerStore.prototype, "getId", null);
__decorate([
    computed
], PlayerStore.prototype, "isComputer", null);
